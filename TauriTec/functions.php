<?php function tauritec_setup() {

add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// shortcode to add buttons to page via editor

function button($atts){
   extract(shortcode_atts(array(
      'link' => '',
      'text' => ''
   ), $atts));

   $get_post = get_post($link);
   $get_post_image = get_the_post_thumbnail_url($link);

   $return_string = "<a class='buttons' title='" . $text . "' href='#modal-" . $link . "'>" . $text . "</a>";
   $return_string .= "<div class='modal-wrapper modal-" . $link . "'>";
   $return_string .= "<div class='modal'>";
   $return_string .= "<div class='modal-close'>x</div>";
   $return_string .= "<div class='modal-image' style='background:url(" . $get_post_image . ") center top no-repeat;background-size:800px;'>";
   $return_string .= "</div>";
   $return_string .= "<div class='modal-text'>";
   $return_string .= $get_post->post_content;
   $return_string .= "</div>";
   $return_string .= "</div>";
   $return_string .= "</div>";

   wp_reset_query();
   return $return_string;
}

   add_shortcode('button', 'button');


}

function tauritec_scripts() {
	wp_enqueue_style( 'normalise', get_template_directory_uri() . '/css/normalise.css',false,'1.1','all');
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/styles.css',false,'1.1','all');
	wp_enqueue_script('scripts', get_template_directory_uri() .'/js/scripts.js', array('jquery'), null, true);
}

add_action( 'wp_enqueue_scripts', 'tauritec_scripts' );

add_action( 'after_setup_theme', 'tauritec_setup' );

?>