jQuery( document ).ready(function() {
	jQuery(".buttons").click(function(e) {
		jQuery(this).next('.modal-wrapper').fadeIn();
		e.preventDefault();
	});

	jQuery(".modal-close").click(function() {
		jQuery('.modal-wrapper').fadeOut();
	});
});